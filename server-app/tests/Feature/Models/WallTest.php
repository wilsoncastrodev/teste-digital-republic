<?php

namespace Tests\Feature\Models;

use App\Models\Wall;
use Tests\TestCase;

class WallTest extends TestCase
{
    public function testCalculateAreaWall()
    {
        $width_wall = 5;
        $height_wall = 10;
        $result_area_wall = 50;

        $area_wall = Wall::calculateAreaWall($width_wall, $height_wall);

        $this->assertEquals($area_wall, $result_area_wall);
    }

    public function testCheckSizeAreaWall()
    {
        $area_wall = 50;

        $this->assertTrue(Wall::checkSizeAreaWall($area_wall));
    }

    public function testCheckHeightPortMinHeightWall()
    {
        $height_wall = 3;
        $ports = 1;

        $this->assertTrue(Wall::checkHeightPortMinHeightWall($ports, $height_wall));
    }

    public function testCheckSizeAreaPortsWindowsMaxWall()
    {
        $area_wall = 30;
        $area_total_ports_windows = 15;

        $this->assertTrue(Wall::checkSizeAreaPortsWindowsMaxWall($area_wall, $area_total_ports_windows));
    }

    public function testCalculateAreaTotalWall()
    {
        $area_wall = 30;
        $area_total_ports_windows = 15;
        $result_area_total_wall = 15;

        $area_total_wall = Wall::calculateAreaTotalWall($area_wall, $area_total_ports_windows);

        $this->assertEquals($area_total_wall, $result_area_total_wall);
    }

    public function testCalculateAreaTotalPortsWindows()
    {
        $area_total_ports = 4.56;
        $area_total_windows = 7.2;
        $result_area_total_ports_windows = 11.76;

        $area_total_ports_windows = Wall::calculateAreaTotalPortsWindows($area_total_ports, $area_total_windows);

        $this->assertEquals($area_total_ports_windows, $result_area_total_ports_windows);
    }
}
