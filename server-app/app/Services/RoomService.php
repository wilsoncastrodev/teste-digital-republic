<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Interfaces\Services\RoomServiceInterface;
use App\Models\Room;
use App\Models\Wall;
use App\Models\Window;
use App\Models\Port;

class RoomService implements RoomServiceInterface
{
    public function calculateAmountPaints(Array $walls): array
    {
        $area_total_walls = 0;

        foreach($walls as $key => $wall) {
            $area_wall = Wall::calculateAreaWall($wall['width'], $wall['height']);

            if(!Wall::checkSizeAreaWall($area_wall)) {
                $errors['wall.' . $key][] = "A área da parede não pode ter menos do que 1m² e nem mais que 50m²";
            }

            if(!Wall::checkHeightPortMinHeightWall($wall['ports'], $wall['height'])) {
                $errors['wall.' . $key][] = "A parede deve ser 30 centímetros maior que a altura da porta";
            }

            $area_total_ports = Port::calculateAreaTotalPorts($wall['ports']);

            $area_total_windows = Window::calculateAreaTotalWindows($wall['windows']);
            $area_total_ports_windows = Wall::calculateAreaTotalPortsWindows($area_total_ports, $area_total_windows);

            if(!Wall::checkSizeAreaPortsWindowsMaxWall($area_wall, $area_total_ports_windows)) {
                $errors['wall.' . $key][] = "O total da área das portas e janelas deve ser no máximo 50% da área da parede";
            }

            $area_total_wall = Wall::calculateAreaTotalWall($area_wall, $area_total_ports_windows);

            $area_total_walls += $area_total_wall;
        }

        if(!empty($errors)) {
            return ['errors' => $errors];
        }
        
        return Room::calculateAmountPaintsRoom($area_total_walls);
    }
}