<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    private static $cans_size = [0.5, 2.5, 3.6, 18];

    public static function calculateAmountPaintsRoom(float $area_total_walls): array
    {
        $liters_paints_total = round($area_total_walls / 5, 2);
        $liters_paints = $liters_paints_total;

        rsort(self::$cans_size);

        $cans[min(self::$cans_size) . 'L'] = 0;

        foreach(self::$cans_size as $size) {
            $cans[$size . 'L'] = 0;

            while($liters_paints > $size) {
                $cans[$size . 'L'] += 1;
                $liters_paints -= $size;
            }

            if($liters_paints > 0 && $liters_paints < min(self::$cans_size)) {
                $cans[min(self::$cans_size) . 'L'] += 1;
            }

            if($cans[$size . 'L'] == 0) {
                unset($cans[$size . 'L']);
            }
        }

        return [
            'area_total_walls' => $area_total_walls,
            'liters_paints_total' => $liters_paints_total,
            'cans' => $cans
        ];
    }
}
