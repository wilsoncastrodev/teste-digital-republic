<?php

namespace Tests\Feature\Models;

use App\Models\Room;
use Tests\TestCase;

class RoomTest extends TestCase
{
    public function testCalculateAmountPaintsRoom()
    {
        $area_total_walls = 200;
        $result_paints = [
            'area_total_walls' => 200,
            'liters_paints_total' => 40,
            'cans' => [
                "18L" => 2,
                "3.6L" => 1,
                "0.5L" => 1
            ]
        ];

        $paints = Room::calculateAmountPaintsRoom($area_total_walls);

        $this->assertEquals($paints, $result_paints);
    }
}
