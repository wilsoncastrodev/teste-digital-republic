#!/bin/bash -it

cp .env.example .env
composer install
php artisan route:clear
php artisan cache:clear
php artisan config:clear
php artisan key:generate
chmod -R 777 storage
