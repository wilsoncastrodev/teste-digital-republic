<?php

namespace Tests\Feature\Models;

use App\Models\Window;
use Tests\TestCase;

class WindowTest extends TestCase
{
    public function testCalculateAreaTotalWindows()
    {
        $windows = 3;
        $result_area_total_windows = 7.2;

        $area_total_windows = Window::calculateAreaTotalWindows($windows);

        $this->assertEquals($area_total_windows, $result_area_total_windows);
    }
}
