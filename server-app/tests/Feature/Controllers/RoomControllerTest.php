<?php

namespace Tests\Feature\Controllers;

use Tests\TestCase;

class RoomControllerTest extends TestCase
{
    public function testCalculateAmountPaintsApi()
    {
        $walls = [
            'walls' => [
                [
                    'height' => 5,
                    'width' => 10,
                    'ports' => 1,
                    'windows' => 1
                ],
                [
                    'height' => 5,
                    'width' => 10,
                    'ports' => 1,
                    'windows' => 1
                ],
                [
                    'height' => 5,
                    'width' => 10,
                    'ports' => 1,
                    'windows' => 1
                ],
                [
                    'height' => 5,
                    'width' => 10,
                    'ports' => 1,
                    'windows' => 2
                ]
            ]
        ];

        $response = $this->post('api/room/calculate-paints', $walls);
        $response->assertStatus(200);
        $response->assertOk();
    }
}
