<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CalculatePaintsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'walls.*.height' => 'required',
            'walls.*.width' => 'required',
            'walls.*.ports' => 'required',
            'walls.*.windows' => 'required',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'walls.*.height.required' => 'O campo "Altura" da parede é obrigatório',
            'walls.*.width.required' => 'O campo "Largura" da parede é obrigatório',
            'walls.*.ports.required' => 'O campo "Quantidade de Portas" é obrigatório',
            'walls.*.windows.required' => 'O campo "Quantidades de Janelas" é obrigatório',
        ];
    }
}
