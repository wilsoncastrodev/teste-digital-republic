<?php

namespace Tests\Feature\Models;

use App\Models\Port;
use Tests\TestCase;

class PortTest extends TestCase
{
    public function testCalculateAreaTotalPorts()
    {
        $ports = 3;
        $result_area_total_ports = 4.56;

        $area_total_ports = Port::calculateAreaTotalPorts($ports);

        $this->assertEquals($area_total_ports, $result_area_total_ports);
    }
}
