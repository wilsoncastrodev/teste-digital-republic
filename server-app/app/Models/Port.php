<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Port extends Model
{
    private static $width_port = 0.80;
    private static $height_port = 1.90;

    public static function calculateAreaTotalPorts(int $ports): float
    {
        return round($ports * self::$width_port * self::$height_port, 2);
    }
}
