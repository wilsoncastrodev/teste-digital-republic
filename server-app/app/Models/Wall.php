<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wall extends Model
{
    private static $area_wall_min = 1;
    private static $area_wall_max = 50;
    private static $height_port = 1.90;
    
    public static function calculateAreaWall(float $width_wall, float $height_wall): float
    {
        return $width_wall * $height_wall;
    }

    public static function checkSizeAreaWall(float $area_wall): bool
    {
        return $area_wall < self::$area_wall_min || $area_wall > self::$area_wall_max ? false : true;
    }
    
    public static function checkHeightPortMinHeightWall(int $ports, float $height_wall): bool
    {
        return $ports && $height_wall < self::$height_port + 0.30 ? false : true;
    }
    
    public static function checkSizeAreaPortsWindowsMaxWall(float $area_wall, float $area_total_ports_windows): bool
    {
        return $area_wall * 0.50 < $area_total_ports_windows ? false : true;
    }

    public static function calculateAreaTotalWall(float $area_wall, float $area_total_ports_windows): float
    {
        return $area_wall - $area_total_ports_windows;
    }

    public static function calculateAreaTotalPortsWindows(float $area_total_ports, float $area_total_windows): float
    {
        return $area_total_ports + $area_total_windows;
    }
}
