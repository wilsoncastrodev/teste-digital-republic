export type PaintResponseType = {
    area_total_walls: number,
    liters_paints_total: number,
    cans: any
};

export type PaintRequestType = {
    walls: any
}

export type PaintStateType = {
    paints: PaintResponseType | any,
    errors: any,
    isLoading: boolean,
};
