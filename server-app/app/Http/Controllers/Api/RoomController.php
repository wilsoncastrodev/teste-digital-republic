<?php

namespace App\Http\Controllers\Api;

use App\Services\RoomService;
use App\Http\Controllers\Controller;
use App\Http\Requests\CalculatePaintsRequest;

class RoomController extends BaseController
{
    private RoomService $roomService;

    public function __construct(RoomService $roomService)
    {
        $this->roomService = $roomService;
    }
    
    public function calculateAmountPaints(CalculatePaintsRequest $request)
    {
        try {
            $paints = $this->roomService->calculateAmountPaints($request->walls);
            return empty($paints['errors']) 
                   ? $this->sendResponse($paints) 
                   : $this->sendError('Erro na validação dos dados', $paints['errors'], 422);
        } catch (Exception $e) {
            return $this->sendErrorException($e);
        }
    }
}
