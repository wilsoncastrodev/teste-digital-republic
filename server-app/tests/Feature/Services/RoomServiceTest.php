<?php

namespace Tests\Feature\Services;

use App\Services\RoomService;
use Tests\TestCase;

class RoomServiceTest extends TestCase
{
    public function setUp() : void
    {
        parent::setUp();
        $this->roomService = app()->make(RoomService::class);
    }

    public function testCalculateAmountPaints()
    {
        $walls = [
            [
                'height' => 5,
                'width' => 10,
                'ports' => 1,
                'windows' => 1
            ],
            [
                'height' => 5,
                'width' => 10,
                'ports' => 1,
                'windows' => 1
            ],
            [
                'height' => 5,
                'width' => 10,
                'ports' => 1,
                'windows' => 1
            ],
            [
                'height' => 5,
                'width' => 10,
                'ports' => 1,
                'windows' => 2
            ]
        ];

        $paints = $this->roomService->calculateAmountPaints($walls);

        $this->assertArrayHasKey('area_total_walls', $paints);
        $this->assertArrayHasKey('liters_paints_total', $paints);
        $this->assertArrayHasKey('cans', $paints);
    }
}
