
# Teste - Empresa DIGITAL REPUBLIC - Vaga Desenvolvedor(a) PHP/Node

![PHP](https://img.shields.io/badge/php-%23777BB4.svg?style=for-the-badge&logo=php&logoColor=white)  ![Laravel](https://img.shields.io/badge/laravel-%23FF2D20.svg?style=for-the-badge&logo=laravel&logoColor=white)  ![JavaScript](https://img.shields.io/static/v1?style=for-the-badge&message=JavaScript&color=222222&logo=JavaScript&logoColor=F7DF1E&label=)  ![TypeScript](https://img.shields.io/static/v1?style=for-the-badge&message=TypeScript&color=3178C6&logo=TypeScript&logoColor=FFFFFF&label=)  ![React](https://img.shields.io/static/v1?style=for-the-badge&message=React&color=222222&logo=React&logoColor=61DAFB&label=)  ![Redux](https://img.shields.io/static/v1?style=for-the-badge&message=Redux&color=764ABC&logo=Redux&logoColor=FFFFFF&label=)  ![HTML5](https://img.shields.io/static/v1?style=for-the-badge&message=HTML5&color=E34F26&logo=HTML5&logoColor=FFFFFF&label=)  ![CSS3](https://img.shields.io/static/v1?style=for-the-badge&message=CSS3&color=1572B6&logo=CSS3&logoColor=FFFFFF&label=)  ![Sass](https://img.shields.io/static/v1?style=for-the-badge&message=Sass&color=CC6699&logo=Sass&logoColor=FFFFFF&label=)  ![Nginx](https://img.shields.io/badge/nginx-%23009639.svg?style=for-the-badge&logo=nginx&logoColor=white)  ![Docker](https://img.shields.io/static/v1?style=for-the-badge&message=Docker&color=2496ED&logo=Docker&logoColor=FFFFFF&label=)  ![Git](https://img.shields.io/static/v1?style=for-the-badge&message=Git&color=F05032&logo=Git&logoColor=FFFFFF&label=)  ![GitLab](https://img.shields.io/static/v1?style=for-the-badge&message=GitLab&color=FC6D26&logo=GitLab&logoColor=FFFFFF&label=)  ![Linux](https://img.shields.io/static/v1?style=for-the-badge&message=Linux&color=222222&logo=Linux&logoColor=FCC624&label=)

## Objetivo
Uma aplicação web ou mobile que ajude o usuário a calcular a quantidade de tinta necessária para pintar uma sala.
Essa aplicação deve considerar que a sala é composta de 4 paredes e deve permitir que o usuário escolha qual a medida de cada parede e quantas janelas e portas possuem cada parede. Com base na quantidade necessária o sistema deve apontar tamanhos de lata de tinta que o usuário deve comprar, sempre priorizando as latas maiores.

![Challenge - Completed](https://img.shields.io/badge/Challenge-Completed-31c754) ![PHPUnit - Passing](https://img.shields.io/badge/PHPUnit-Passing-31c754) ![Tests - 11 passed](https://img.shields.io/badge/Tests-11_passed-31c754) ![Commits - 16](https://img.shields.io/badge/Commits-16-23bbca)

## Instruções do Teste
- [Clique aqui](https://gitlab.com/wilsoncastrodev/teste-digital-republic/-/blob/master/TESTE.md) para visualizar as instruções do teste

## Índice
- [Demonstração](#demonstração)
- [Funcionalidades Implementadas](#funcionalidades-implementadas)
- [Informações Adicionais](#informações-adicionais)
- [Instalação e Execução da Aplicação](#instalação-e-execução-da-aplicação)
- [Documentação da API](#documentação-da-api)
- [Screenshots Aplicação](#screenshots-aplicação)
- [Screenshots Testes Unitários](#screenshots-testes-unitários)
- [Tecnologias Utilizadas](#tecnologias-utilizadas)
- [Referências](#referências)
- [Agradecimentos](#agradecimentos)
- [Contato](#contato)
- [Autores](#autores)
- [Licença](#licença)

## Demonstração
Caso queira ver a aplicação em execução sem a necessidade de instalá-la em sua máquina local, acesse: https://digital-republic.tests.wilsoncastro.dev

## Funcionalidades Implementadas
- Cálculo da quantidade de tinta necessária para pintar uma sala;

## Informações Adicionais
- As validações do cálculo da quantidade tinta foram realizados no back-end e no front-end;
- Foram utilizados Docker e Docker Compose para desenvolvimento e execução do projeto;
- Foi utilizado o padrão de projeto Service para implementação das regras de negócio;
- Foram utilizados testes unitários com PHPUnit para validar o comportamento e o funcionamento da aplicação;
- Busquei tentar utilizar no desafio a maioria das tecnologias descritas na vaga de desenvolvedor Desenvolvedor(a) PHP/Node

## Instalação e Execução da Aplicação

#### Pré-requisitos
São necessários os seguintes requisitos instalados na máquina local para testar a aplicação:
- Docker
- Docker Compose

### Instalando e executando o projeto com o Docker
- Clone o repositório:

```bash
  git clone https://gitlab.com/wilsoncastrodev/teste-digital-republic.git
```
- Em seguida, entre no diretório raiz do projeto utilizando o comando a seguir:

```bash
  cd teste-digital-republic
```
- Execute o seguinte comando para iniciar a execução dos containers do Docker:

```bash
  docker-compose up -d
```
- Caso o servidor esteja sendo executado pela primeira vez é necessário rodar o comando abaixo, para carregar as dependências e realizar as configurações necessárias para que aplicação funcione corretamente:

```bash
  docker exec -it digital-republic-server-app-php bash ../server-init.sh
```
- Se a instalação tiver ocorrido normalmente, abra o navegador e execute a URL a seguir:

```bash
  http://localhost:8500
```

- Pronto!

## Rodando os testes

- Para rodar os testes, rode o seguinte comando:

```bash
  docker exec -it digital-republic-server-app-php php artisan test
```

## Documentação da API

### Sala

#### Endpoints
| Método   | Rota       | Descrição                                   |
| :---------- | :--------- | :------------------------------------------ |
| `POST`      | `api/room/calculate-paints` | Calcula a quantidade de tintas para pintar uma sala |

#### Calcula a quantidade de tintas necessárias para pintar uma sala

```
  POST /api/calculate-paints
```

Exemplo de corpo de Requisição:
```json
{
    "walls": [
        {
            "height": 5,
            "width": 10,
            "ports": 2,
            "windows": 1
        },
        {
            "height": 3,
            "width": 10,
            "ports": 1,
            "windows": 1
        },
        {
            "height": 5,
            "width": 6,
            "ports": 0,
            "windows": 0
        },
        {
            "height": 2,
            "width": 7,
            "ports": 0,
            "windows": 0
        }
    ]
}
```

Exemplo de Resposta:

```json
{
    "data": {
        "area_total_walls": 114.64,
        "liters_paints_total": 22.93,
        "cans": {
            "18L": 1,
            "3.6L": 1,
            "0.5L": 3
        }
    }
}
```

## Screenshots Aplicação

![App Screenshot](https://gitlab.com/wilsoncastrodev/teste-digital-republic/-/raw/feature/ajustes-finais/client-app/public/images/app/imagem1.png)

![App Screenshot](https://gitlab.com/wilsoncastrodev/teste-digital-republic/-/raw/feature/ajustes-finais/client-app/public/images/app/imagem2.png)

## Screenshots Testes Unitários

![App Screenshot](https://gitlab.com/wilsoncastrodev/teste-digital-republic/-/raw/feature/ajustes-finais/client-app/public/images/tests/imagem1.png)

## Tecnologias Utilizadas

| Tecnologia | Versão
| :---: | :---: |
| PHP| 8.x |
| Laravel | 10.x |
| Nginx | 1.x |
| Javascript | - |
| Typescript | 5.x |
| React | 18.x |
| Redux | 4.x |
| HTML5 | - |
| CSS3 | - |
| Docker (Docker Compose) | 23.x |
| APIs REST/RESTful | - |
| Git | - |

## Referências
 - [Documentação do Laravel](https://laravel.com/docs/10.x/)
 - [Documentação do React](https://pt-br.reactjs.org/)

## Agradecimentos
Agradeço a equipe da Digital Republic por ter desenvolvido este desafio e pela oportunidade de concorrer à vaga de desenvolvedor PHP/Node. Que Deus possa abençoá-los.

## Contato
Qualquer coisa, sinta-se à vontade para entrar em contato comigo pelo e-mail contato@wilsoncastro.dev.

## Autores
- [@wcastro](https://github.com/wilsoncastrodev)

## Licença
[![MIT License](https://img.shields.io/badge/License-MIT-green.svg)](https://choosealicense.com/licenses/mit/)