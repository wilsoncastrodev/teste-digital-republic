<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Window extends Model
{
    private static $width_window = 2.00;
    private static $height_window = 1.20;

    public static function calculateAreaTotalWindows(int $windows): float
    {
        return round($windows * self::$width_window * self::$height_window, 2);
    }
}
