<?php

namespace App\Interfaces\Services;

use Illuminate\Http\Request;

interface RoomServiceInterface
{
    /**
     * Serviço para calcular a quantidade de tintas necessárias para pintar uma sala
     *
     * @param Request $request Objeto Quarto da Requisição
     *
     * @return array Contêm os dados do cálculo.
     */
    public function calculateAmountPaints(Array $walls): array;
}
